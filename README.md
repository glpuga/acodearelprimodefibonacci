# CITES Coding Contest 2016

## Problema

Esta carpeta contiene la solución al problema "El Primo de Fibonacci". A partir de la consigna era necesario responder:
  1. ¿Para qué valores de n ≤ 90 se da que f(n) es primo?
  2. ¿Cuántas cifras tiene el valor f(1477) escrito en base diez?

## Pregunta 1

#### Solución
Además de f(1) y f(2), son primos los siguientes números f(n) de la secuencia fibonacci, para n < 90.

	fib(3) = 2 
	fib(4) = 3 
	fib(5) = 5 
	fib(7) = 13 
	fib(11) = 89 
	fib(13) = 233 
	fib(17) = 1597 
	fib(23) = 28657 
	fib(29) = 514229 
	fib(43) = 433494437 
	fib(47) = 2971215073 
	fib(83) = 99194853094755497

#### Metodología

Previo a comenzar se verificó que los números de la serie Fibonacci hasta fib(90) pueden almacenarse en variables enteras de anchos de palabra de más de 32 bits pero menos de 64, por lo que el problema se podía encarar realizando un programa en C con variables int64_t.

Entonces para obtener la solución se escribió en C el programa fibo1.c, el cual calcula los primeros 90 números de la secuencia Fibonacci y para cada uno verifica su primalidad con un algoritmo eficiente. 


## Pregunta 2

#### Solución

El número fib(1477) tiene 309 dígitos:

	fib(1477) [309 dígitos] = <211475298697902185255785861961179135570552502746803252174956226558634024323947666637137823932524397611864671566211908330263377425204552074188208686993669123754004340250943108709212299180422293009765404930508242975773774612140021599477983006713536106549441161323499077298115887067363710153036315849480388057657>

###  Metodología

Debido a que la cantidad de bits necesarios para almacenar los números de la serie Fibonacci con valores de n elevados no permite trabajar con variables de tipos enteros normales, para este problema se opto por escribir el problema fibo2.c que define un tipo de datos especial para almacenar números de ancho arbitrario, junto con los operadores necesarios para operar con este tipo de dato.

De esa forma el programa puede derivar los números de la secuencia Fibonacci hasta valores de n arbitrariamente altos, sólo limitado por la memoria disponible en el sistema.

## Cómo ejecutar las Soluciones

Para ejecutar las soluciones se deben compilar utilizando el Makefila incluido con los archivos de la solución, y ejecutar los programas fibo1 y fibo2.
    
    # make clean
    # make all
    # ./fibo1
    # ./fibo2

## Repositorio Git de la solución

Todos los archivos de la solución pueden hallarse en

https://gitlab.com/glpuga/acodearelprimodefibonacci.git




