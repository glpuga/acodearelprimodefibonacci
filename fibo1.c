#include <stdio.h>
#include <stdint.h>

#define STOPAT 90

int64_t checkFiboPrimality(int64_t candidate)
{
    int32_t bits;
    int64_t divisor;
    uint64_t shiftRegister;
    uint64_t sqrtUpperBound;

    /* el algoritmo siguiente tiene dos casos especiales, que son
       el 2 y el 3, que deben verificarse por separado. */
    if ((candidate == 2) || (candidate == 3))
    {
	return 0;
    }

    /* Si el número es par, ya no es primo */
    if (candidate % 2 == 0)
    {
	return 2;
    }

    /* Habiendo descartado eso, sólo necesito verificar los divisores posibles, impares, a partir del 3 */
    divisor = 3;

    /* Por eficiencia computacional, no necesito verificar nada más allá de sqrt(candidate),
       porque si existiera un divisor más grande que eso, significa que necesariamente tenía que haber
       uno más chico también, que es algo que ya descarté.

	Sin embargo, en un segundo pensamiento, como no tengo garantías de poder representar la cantidad de
        dígitos de los fib(n) superiores en un double sin exceder la representación y tener un error de 
        magnitud desconocida, prefiero buscar una cota superior a la raíz cuadrada a partir 
	de determinar la cantidad de bits que tiene el número:  si el número candidato tiene n bits, 
        su raiz cuadrada no puede tener más de la mitad.  

	Notar que una aproximación todavía más eficiente sería verificar la divisibilidad del número 
	candidato solamente por los primos inferiores ya encontrados (criba de Erastótenes), pero
	esa aproximación acá no es posible porque no estoy buscando TODOS los primos, sino solamente
	verificando puntualmente aquellos que forman parte de la serie Fibonacci. */

    /* Cuento la cantidad de bits que tiene el número */
    bits           = 0;
    shiftRegister  = (uint64_t)candidate;

    while (shiftRegister != 0)
    {
	/* Cuento la cantidad de bits */
	bits++;
	shiftRegister = shiftRegister >> 1;
    }

    /* Genero la cota de valor que puede tener la raíz cuadrada,  creando el número (2^(bits / 2) - 1) */
    bits           = (bits + 1) / 2; /* el +1 es para redondear para arriba */
    sqrtUpperBound = 0;

    while (bits != 0)
    {
	sqrtUpperBound = (sqrtUpperBound << 1) | 1;
	bits--;
    }

    /* Ahora sí, verifico la divisibilidad */
    while (divisor <= sqrtUpperBound)
    {
	/* Verifico si el divisor actual divide exactamente el número */
	if (candidate % divisor == 0)
	{
	    return divisor;
	}
    
	/* avanzo el divisor al siguiente impar */
	divisor = divisor + 2;
    }

    /* Si salí por acá es que verifiqué todos los divisores posibles y ninguno dividía el número */
    return 0;
}

int main(void)
{
    int64_t fibo1;
    int64_t fibo2;
    int64_t nextFibo;
    int32_t nextIndex;
    int64_t divisor;

    /* Iniciliazo la serie */
    fibo1 = 1;
    fibo2 = 1;

    printf("\n");
    printf("Calculando los primeros %d números de la serie Fibonacci y probando su primalidad\n", STOPAT);
    printf("\n");
    for (nextIndex = 3; nextIndex <= STOPAT; nextIndex++)
    {
	/* Calculo el número siguiente de la serie */
	nextFibo = fibo1 + fibo2;

	/* Busco los divisores. Si devuelve cero es que no tiene, sino devuelve el divisor. */
	divisor = checkFiboPrimality(nextFibo);

	/* Imprimo el número */
	if (divisor == 0)
	{
	    printf("fib(%d) = %lld (ES PRIMO)\n", nextIndex, (long long int)nextFibo);
	} else {
	    printf("fib(%d) = %lld (divide por %lld)\n", nextIndex, (long long int)nextFibo, (long long int)divisor);
	}

	/* Avanzo la serie */
	fibo2 = fibo1;
	fibo1 = nextFibo;
    }

    printf("\n");
    return 0;
}
