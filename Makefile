
RM := rm
CC := gcc
CFLAGS :=

# All Targets
all: fibo1 fibo2

fibo1: fibo1.o
	$(CC) -o fibo1 fibo1.o
	@echo ' '

fibo2: fibo2.o
	$(CC) -o fibo2 fibo2.o
	@echo ' '

clean:
	-$(RM) -f fibo1 fibo2 fibo1.o fibo2.o
	-@echo ' '

%.o : ../src/%.c
	$(CC) -c $(DEFS) $(CFLAGS) $< -o $@

.PHONY: all clean
.SECONDARY:
