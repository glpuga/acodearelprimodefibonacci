
clear
clc

fprintf('\n');

fprintf('Calculando los primeros 90 bits de la serie fibonacci para\n');
fprintf('determinar la cantidad de bits necesarios para representarlo\n');
fprintf('y ver si es factible operar normalmente con ellos para verificar\n');
fprintf('su primalidad.\n');

fprintf('\n');

n = [1:1:90];

f = zeros(size(n));

f(1) = 1;
f(2) = 1;

for I = n(3:end)
    f(I) = f(I-1) + f(I-2);

    fprintf('fib(%d) requiere de %d bits para ser representado.\n', I, ceil(log2(f(I))));
end

fprintf('\n');
