#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define STOPAT 1477

#define FATAL_FAIL(errortxt)   { fprintf(stderr, "Error fatal: %s (%s, linea %d)\n", errortxt, __FILE__, __LINE__); exit(1); }

#define ASSERT(cond, errortxt) { if (!(cond)) FATAL_FAIL(errortxt); }

typedef struct {

    char *buffer;

    int32_t digits;

} longNumberType;


void longNumberInit(longNumberType *number)
{
    number->buffer = NULL;
    number->digits = 0;
}

void longNumberRelease(longNumberType *number)
{
    if (number->digits != 0)
    {
	free(number->buffer);
    }

    number->buffer = NULL;
    number->digits = 0;
}

void longNumberSet(longNumberType *number, uint32_t value)
{
    uint32_t bound;
    uint32_t digits;
    uint32_t auxValue;
    int32_t index;
    
    /* Determino cuántos dígitos tiene el número */

    bound = 10;
    digits = 1;

    while (value >= bound) 
    {
	bound = bound * 10;
	digits++;
    }

    printf("Set with %d digits\n", digits);	

    /* Reservo memoria en el buffer */
    number->buffer = malloc(digits);
    number->digits = digits;

    /* chequeo de seguridad */
    ASSERT(number->buffer != NULL, "No hay suficiente memoria!");

    /* Copio los dígitos a la estructura */
    auxValue = value;
    index    = 0;

    while(auxValue != 0)
    {
        ASSERT(index < digits, "Error al determinar la cantidad de dígitos!");

	number->buffer[index] = (char) (auxValue % 10);

	auxValue = auxValue / 10;
	
	index++;
    }
}

void longNumberCopy(longNumberType *dst, longNumberType *src)
{
    /* Libero cualquier memoria que pueda tener en uso la estructura de destino */
    longNumberRelease(dst);

    /* Reservo memoria en el buffer */
    dst->buffer = malloc(src->digits);
    dst->digits = src->digits;

    /* chequeo de seguridad */
    ASSERT(dst->buffer != NULL, "No hay suficiente memoria!");

    /* copio el contenido de un buffer al otro */
    memcpy((void*)dst->buffer, (void*)src->buffer, src->digits);
}



void longNumberIncreaseLength(longNumberType *num, int32_t increment)
{
    longNumberType auxNum;
    int32_t index;

    /* Reservo memoria en el buffer */
    auxNum.buffer = malloc(num->digits + increment);
    auxNum.digits = num->digits + increment;

    /* chequeo de seguridad */
    ASSERT(auxNum.buffer != NULL, "No hay suficiente memoria!");

    /* copio el contenido de un buffer al otro */
    memcpy((void*)auxNum.buffer, (void*)num->buffer, num->digits);

    /* inicializo en cero los bytes incrementados */
    for (index = num->digits; index < auxNum.digits; index++)
    {
	auxNum.buffer[index] = 0;
    }

    /* Copio el nuevo número en el original */
    longNumberCopy(num, &auxNum);
}


void longNumberAdd(longNumberType *numA, longNumberType *numB, longNumberType *dst)
{
    int32_t digits;
    int32_t carry;
    int32_t index;
    int32_t digA, digB, digSum;

    /* Libero cualquier memoria que pueda tener en uso la estructura de destino */
    longNumberRelease(dst);

    /* Reservo espacio en el número destino para los dígitos que voy a necesitar. El
       resultado de la suma puede tener a lo sumo un dígito más que el máximo de los dos. En principio
       trato de no usar el dígito extra, y sólo agregarlo si tengo carry en la última suma*/

    digits = numA->digits;

    if (numB->digits > digits)
    {
	digits = numB->digits;
    }

    /* Reservo memoria en el buffer */
    dst->buffer = malloc(digits);
    dst->digits = digits;

    /* chequeo de seguridad */
    ASSERT(dst->buffer != NULL, "No hay suficiente memoria!");

    /* hago la suma dígito a dígito */
    carry = 0;

    for (index = 0; index < digits; index++)
    {
	if (index < numA->digits)
	{
	    digA = numA->buffer[index];
	} else {
	    digA = 0;
	}

	if (index < numB->digits)
	{
	    digB = numB->buffer[index];
	} else {
	    digB = 0;
	}

	/* Sumo los dígitos */
	digSum = digA + digB + carry;
	carry  = 0;

	/* Verifico si hay acarreo al siguiente dígito */
	if (digSum >= 10)
	{
	    digSum = digSum - 10;
	    carry = 1;
	}
	
	/* Almaceno el resultado en el número destino */
	dst->buffer[index] = (char) digSum;
    }

    /* Si hay acarreo tengo que alargar el resultado en un dígito */
    if (carry != 0)
    {	
        longNumberIncreaseLength(dst, 1);
        dst->buffer[index] = carry;
    }
}

void longNumberPrint(longNumberType *number)
{
    int32_t index;
    
    if (number->digits == 0)
    {
	printf("<NONUM!>");
	return;
    }

    printf("<");
    for (index = 0; index < number->digits; index++)	
    {
	printf("%c", number->buffer[number->digits - index - 1] + '0');
    }
    printf(">");
}

int main(void)
{
    longNumberType fibo1, fibo2;
    int32_t nextIndex;

    longNumberType nextFibo;

    longNumberInit(&fibo1);
    longNumberInit(&fibo2);
    longNumberInit(&nextFibo);

    longNumberSet(&fibo1, 1);
    longNumberSet(&fibo2, 1);

    printf("\n");
    printf("Calculando los primeros %d números de la serie Fibonacci y probando su primalidad\n", STOPAT);
    printf("\n");

    for (nextIndex = 3; nextIndex <= STOPAT; nextIndex++)
    {
	/* Calculo el número siguiente de la serie */
        longNumberAdd(&fibo1, &fibo2, &nextFibo);

	/* Imprimo el número */
	printf("fib(%d) [%d dígitos] = ", (int)nextIndex, (int) nextFibo.digits);
	longNumberPrint(&nextFibo);
	printf("\n");

	longNumberCopy(&fibo2, &fibo1);
	longNumberCopy(&fibo1, &nextFibo);
    }

    printf("\n");
    return 0;
}
